'use strict';

angular.module('pdmApp', ['ngMaterial', 'ngAnimate', 'ui.bootstrap', 'smart-table', 'pdmApp.filters', 'pdmApp.services', 'pdmApp.controllers', 'pdmApp.directives']);